package com.vegagroup.apiproviderproduct.dto.Response;

public class OfferResponse {
	
	private int result;
    private String errorDescription;
    private Object OfferInfo;
	
    public OfferResponse(int result, String errorDescription, Object offerInfo) {
		super();
		this.result = result;
		this.errorDescription = errorDescription;
		OfferInfo = offerInfo;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	public String getErrorDescription() {
		return errorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	public Object getOfferInfo() {
		return OfferInfo;
	}

	public void setOfferInfo(Object offerInfo) {
		OfferInfo = offerInfo;
	}
    
    
    
}
