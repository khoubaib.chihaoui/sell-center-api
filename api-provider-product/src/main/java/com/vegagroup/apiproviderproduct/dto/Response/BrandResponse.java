package com.vegagroup.apiproviderproduct.dto.Response;

public class BrandResponse {

	private int result;
    private String errorDescription;
    private Object BrandInfo;
    
    
    
    
	public BrandResponse(int result, String errorDescription, Object brandInfo) {
		super();
		this.result = result;
		this.errorDescription = errorDescription;
		BrandInfo = brandInfo;
	}
	public int getResult() {
		return result;
	}
	public void setResult(int result) {
		this.result = result;
	}
	public String getErrorDescription() {
		return errorDescription;
	}
	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}
	public Object getBrandInfo() {
		return BrandInfo;
	}
	public void setBrandInfo(Object brandInfo) {
		BrandInfo = brandInfo;
	}
    
    
}
