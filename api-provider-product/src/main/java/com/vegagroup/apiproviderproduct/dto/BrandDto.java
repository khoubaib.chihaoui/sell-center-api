package com.vegagroup.apiproviderproduct.dto;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class BrandDto {

	
    private String name;
    
    private String picture;
    
    

	public BrandDto(String name, String picture) {
		super();
		this.name = name;
		this.picture = picture;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}
    
    
}


