package com.vegagroup.apiproviderproduct.dto;

import java.util.Date;

import model.Category;




import java.util.Date;


public class CategoryDto {

    private Integer categoryid;
    private String picture;
    private Short isdeleted; 
	private Date deleteddate;
    private Date addeddate;
    private CategoryDto catCategoryid;
    private String name;
    private Short isactif;

    public CategoryDto(Integer categoryid, String picture, Short isdeleted, Date deleteddate, Date addeddate, CategoryDto catCategoryid, String name, Short isactif) {
        this.categoryid = categoryid;
        this.picture = picture;
        this.isdeleted = isdeleted;
        this.deleteddate = deleteddate;
        this.addeddate = addeddate;
        this.catCategoryid = catCategoryid;
        this.name = name;
        this.isactif = isactif;
    }

    public CategoryDto() {
    }

    public Integer getCategoryid() {
        return categoryid;
    }

    public void setCategoryid(Integer categoryid) {
        this.categoryid = categoryid;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Short getIsdeleted() {
        return isdeleted;
    }

    public void setIsdeleted(Short isdeleted) {
        this.isdeleted = isdeleted;
    }

    public Date getDeleteddate() {
        return deleteddate;
    }

    public void setDeleteddate(Date deleteddate) {
        this.deleteddate = deleteddate;
    }

    public Date getAddeddate() {
        return addeddate;
    }

    public void setAddeddate(Date addeddate) {
        this.addeddate = addeddate;
    }

    public CategoryDto getCatCategoryid() {
        return catCategoryid;
    }

    public void setCatCategoryid(CategoryDto catCategoryid) {
        this.catCategoryid = catCategoryid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Short getIsactif() {
        return isactif;
    }

    public void setIsactif(Short isactif) {
        this.isactif = isactif;
    }
}
