package com.vegagroup.apiproviderproduct.dto;

public class OfferDto {

	private Integer offerid;
	private String title;
	private String description;
	private Short discount;
	private Short tranchepayment;
	private Short cashpayment;
	private Short typediscount;
	private Short typedeadline;
	private Short data;
	private Double priceoffer;
	private Integer nbrperson;
	private Integer nbrfamily;
	private String poster;
	private Short isactif;
	private Boolean isDeleted;
	private Boolean hided;
	private Short maxoffer;
	private String offeridstring;
	private String service_id;
	private Short withoutautovalidation;
	private Short withdelivery;
	private String supportnum;
	private Short payondelivery;
	private Double deliveryamount;
	private Short withreservation;
	
	
	
	
	
	
	public OfferDto(Integer offerid, String title, String description, Short discount, Short tranchepayment,
			Short cashpayment, Short typediscount, Short typedeadline, Short data, Double priceoffer, Integer nbrperson,
			Integer nbrfamily, String poster, Short isactif, Boolean isDeleted, Boolean hided, Short maxoffer,
			String offeridstring, String service_id, Short withoutautovalidation, Short withdelivery, String supportnum,
			Short payondelivery, Double deliveryamount, Short withreservation) {
		super();
		this.offerid = offerid;
		this.title = title;
		this.description = description;
		this.discount = discount;
		this.tranchepayment = tranchepayment;
		this.cashpayment = cashpayment;
		this.typediscount = typediscount;
		this.typedeadline = typedeadline;
		this.data = data;
		this.priceoffer = priceoffer;
		this.nbrperson = nbrperson;
		this.nbrfamily = nbrfamily;
		this.poster = poster;
		this.isactif = isactif;
		this.isDeleted = isDeleted;
		this.hided = hided;
		this.maxoffer = maxoffer;
		this.offeridstring = offeridstring;
		this.service_id = service_id;
		this.withoutautovalidation = withoutautovalidation;
		this.withdelivery = withdelivery;
		this.supportnum = supportnum;
		this.payondelivery = payondelivery;
		this.deliveryamount = deliveryamount;
		this.withreservation = withreservation;
	}
	public String getPoster() {
		return poster;
	}
	public void setPoster(String poster) {
		this.poster = poster;
	}
	public Short getIsactif() {
		return isactif;
	}
	public void setIsactif(Short isactif) {
		this.isactif = isactif;
	}
	public Boolean getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	public Boolean getHided() {
		return hided;
	}
	public void setHided(Boolean hided) {
		this.hided = hided;
	}
	public Short getMaxoffer() {
		return maxoffer;
	}
	public void setMaxoffer(Short maxoffer) {
		this.maxoffer = maxoffer;
	}
	public String getOfferidstring() {
		return offeridstring;
	}
	public void setOfferidstring(String offeridstring) {
		this.offeridstring = offeridstring;
	}
	public String getService_id() {
		return service_id;
	}
	public void setService_id(String service_id) {
		this.service_id = service_id;
	}
	public Short getWithoutautovalidation() {
		return withoutautovalidation;
	}
	public void setWithoutautovalidation(Short withoutautovalidation) {
		this.withoutautovalidation = withoutautovalidation;
	}
	public Short getWithdelivery() {
		return withdelivery;
	}
	public void setWithdelivery(Short withdelivery) {
		this.withdelivery = withdelivery;
	}
	public String getSupportnum() {
		return supportnum;
	}
	public void setSupportnum(String supportnum) {
		this.supportnum = supportnum;
	}
	public Short getPayondelivery() {
		return payondelivery;
	}
	public void setPayondelivery(Short payondelivery) {
		this.payondelivery = payondelivery;
	}
	public Double getDeliveryamount() {
		return deliveryamount;
	}
	public void setDeliveryamount(Double deliveryamount) {
		this.deliveryamount = deliveryamount;
	}
	public Short getWithreservation() {
		return withreservation;
	}
	public void setWithreservation(Short withreservation) {
		this.withreservation = withreservation;
	}
	public Integer getOfferid() {
		return offerid;
	}
	public void setOfferid(Integer offerid) {
		this.offerid = offerid;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Short getDiscount() {
		return discount;
	}
	public void setDiscount(Short discount) {
		this.discount = discount;
	}
	public Short getTranchepayment() {
		return tranchepayment;
	}
	public void setTranchepayment(Short tranchepayment) {
		this.tranchepayment = tranchepayment;
	}
	public Short getCashpayment() {
		return cashpayment;
	}
	public void setCashpayment(Short cashpayment) {
		this.cashpayment = cashpayment;
	}
	public Short getTypediscount() {
		return typediscount;
	}
	public void setTypediscount(Short typediscount) {
		this.typediscount = typediscount;
	}
	public Short getTypedeadline() {
		return typedeadline;
	}
	public void setTypedeadline(Short typedeadline) {
		this.typedeadline = typedeadline;
	}
	public Short getData() {
		return data;
	}
	public void setData(Short data) {
		this.data = data;
	}
	public Double getPriceoffer() {
		return priceoffer;
	}
	public void setPriceoffer(Double priceoffer) {
		this.priceoffer = priceoffer;
	}
	public Integer getNbrperson() {
		return nbrperson;
	}
	public void setNbrperson(Integer nbrperson) {
		this.nbrperson = nbrperson;
	}
	public Integer getNbrfamily() {
		return nbrfamily;
	}
	public void setNbrfamily(Integer nbrfamily) {
		this.nbrfamily = nbrfamily;
	}
	
	
}
