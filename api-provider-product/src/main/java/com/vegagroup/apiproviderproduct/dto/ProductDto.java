package com.vegagroup.apiproviderproduct.dto;




import java.util.Date;

/**
 * Created by ahmed on 17/07/2019.
 */

public class ProductDto {
	private Integer productid;
	private String name;
	private String description;
	private String payments;
	private Short serialized;
	private String sn;
	private String reference;
	private Float stock;
	private Float soldstock;
	private Short isnew;
	private Short ispromotion;
	private Short isdeleted;
	private Short isactif;
	private Date deleteddate;
	private Date addeddate;
	private String pricetype;
	private Float publicprice;
	private Float priceht;
	private Float conventionprice;
	private String specs;
	private Integer terms;
	private Integer waranty;
	private Short displayoutofstock;
	private Float length;
	private Float height;
	private Float depth;
	private Float weight;
	private Short withdelivery;
	private Short freedelivery;
	private Float deliveryfees;
	private Short validation;
	private Date validationdate;
	private String rejectioncause;;
	private String sku;
	public Integer getProductid() {
		return productid;
	}
	public void setProductid(Integer productid) {
		this.productid = productid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPayments() {
		return payments;
	}
	public void setPayments(String payments) {
		this.payments = payments;
	}
	public Short getSerialized() {
		return serialized;
	}
	public void setSerialized(Short serialized) {
		this.serialized = serialized;
	}
	public String getSn() {
		return sn;
	}
	public void setSn(String sn) {
		this.sn = sn;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public Float getStock() {
		return stock;
	}
	public void setStock(Float stock) {
		this.stock = stock;
	}
	public Float getSoldstock() {
		return soldstock;
	}
	public void setSoldstock(Float soldstock) {
		this.soldstock = soldstock;
	}
	public Short getIsnew() {
		return isnew;
	}
	public void setIsnew(Short isnew) {
		this.isnew = isnew;
	}
	public Short getIspromotion() {
		return ispromotion;
	}
	public void setIspromotion(Short ispromotion) {
		this.ispromotion = ispromotion;
	}
	public Short getIsdeleted() {
		return isdeleted;
	}
	public void setIsdeleted(Short isdeleted) {
		this.isdeleted = isdeleted;
	}
	public Short getIsactif() {
		return isactif;
	}
	public void setIsactif(Short isactif) {
		this.isactif = isactif;
	}
	public Date getDeleteddate() {
		return deleteddate;
	}
	public void setDeleteddate(Date deleteddate) {
		this.deleteddate = deleteddate;
	}
	public Date getAddeddate() {
		return addeddate;
	}
	public void setAddeddate(Date addeddate) {
		this.addeddate = addeddate;
	}
	public String getPricetype() {
		return pricetype;
	}
	public void setPricetype(String pricetype) {
		this.pricetype = pricetype;
	}
	public Float getPublicprice() {
		return publicprice;
	}
	public void setPublicprice(Float publicprice) {
		this.publicprice = publicprice;
	}
	public Float getPriceht() {
		return priceht;
	}
	public void setPriceht(Float priceht) {
		this.priceht = priceht;
	}
	public Float getConventionprice() {
		return conventionprice;
	}
	public void setConventionprice(Float conventionprice) {
		this.conventionprice = conventionprice;
	}
	public String getSpecs() {
		return specs;
	}
	public void setSpecs(String specs) {
		this.specs = specs;
	}
	public Integer getTerms() {
		return terms;
	}
	public void setTerms(Integer terms) {
		this.terms = terms;
	}
	public Integer getWaranty() {
		return waranty;
	}
	public void setWaranty(Integer waranty) {
		this.waranty = waranty;
	}
	public Short getDisplayoutofstock() {
		return displayoutofstock;
	}
	public void setDisplayoutofstock(Short displayoutofstock) {
		this.displayoutofstock = displayoutofstock;
	}
	public Float getLength() {
		return length;
	}
	public void setLength(Float length) {
		this.length = length;
	}
	public Float getHeight() {
		return height;
	}
	public void setHeight(Float height) {
		this.height = height;
	}
	public Float getDepth() {
		return depth;
	}
	public void setDepth(Float depth) {
		this.depth = depth;
	}
	public Float getWeight() {
		return weight;
	}
	public void setWeight(Float weight) {
		this.weight = weight;
	}
	public Short getWithdelivery() {
		return withdelivery;
	}
	public void setWithdelivery(Short withdelivery) {
		this.withdelivery = withdelivery;
	}
	public Short getFreedelivery() {
		return freedelivery;
	}
	public void setFreedelivery(Short freedelivery) {
		this.freedelivery = freedelivery;
	}
	public Float getDeliveryfees() {
		return deliveryfees;
	}
	public void setDeliveryfees(Float deliveryfees) {
		this.deliveryfees = deliveryfees;
	}
	public Short getValidation() {
		return validation;
	}
	public void setValidation(Short validation) {
		this.validation = validation;
	}
	public Date getValidationdate() {
		return validationdate;
	}
	public void setValidationdate(Date validationdate) {
		this.validationdate = validationdate;
	}
	public String getRejectioncause() {
		return rejectioncause;
	}
	public void setRejectioncause(String rejectioncause) {
		this.rejectioncause = rejectioncause;
	}
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}

	
    
    
}
