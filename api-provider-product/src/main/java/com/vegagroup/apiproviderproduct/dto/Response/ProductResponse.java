package com.vegagroup.apiproviderproduct.dto.Response;

/**
 * Created by ahmed on 19/07/2019.
 */
public class ProductResponse {

    private int result;
    private String errorDescription;
    private Object productInfo;

    public ProductResponse() {
    }

    public ProductResponse(int result, String errorDescription, Object productInfo) {
        this.result = result;
        this.errorDescription = errorDescription;
        this.productInfo = productInfo;
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public Object getProductInfo() {
        return productInfo;
    }

    public void setProductInfo(Object productInfo) {
        this.productInfo = productInfo;
    }
}
