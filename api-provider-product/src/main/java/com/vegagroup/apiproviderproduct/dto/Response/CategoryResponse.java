package com.vegagroup.apiproviderproduct.dto.Response;

public class CategoryResponse {

	private int result;
	private String errorDescription;
	private Object categoryInfo;

	public CategoryResponse() {

	}

	public CategoryResponse(int result, String errorDescription, Object categoryInfo) {
		super();
		this.result = result;
		this.errorDescription = errorDescription;
		this.categoryInfo = categoryInfo;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	public String getErrorDescription() {
		return errorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	public Object getCategoryInfo() {
		return categoryInfo;
	}

	public void setCategoryInfo(Object categoryInfo) {
		this.categoryInfo = categoryInfo;
	}

}
