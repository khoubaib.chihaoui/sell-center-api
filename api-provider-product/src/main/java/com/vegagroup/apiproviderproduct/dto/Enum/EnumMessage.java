package com.vegagroup.apiproviderproduct.dto.Enum;

import java.util.HashMap;
import java.util.Map;


public enum EnumMessage {

    OFFER_NOT_EXIST("offer non trouvé dans Easylink", -1),
    AMICAL_NOT_EXIST("amical non trouvé dans Easylink", -1),
    
    CATEGORY_NOT_EXIST("category non trouvé dans Easylink", -1),
    CATEGORY_EXIST("category existe déja", -1),
    CATEGORY_NOT_DELETED("categorie non trouvé", -1),
    
    SUBCATEGORY_NOT_EXIST("Sous categorie non trouvé", -1),
    SUBCATEGORY_EXIST("Sous categorie existe déja ", -1),
    
    CONVENTION_NOT_EXIST("convention non trouvé dans Easylink", -1),
    PRODUCT_NOT_EXIST("produt non trouvé dans Easylink", -1),
    
    CATEGORY_ADDED("category ajouté", 0),
    SUBCATEGORY_ADDED("Sous categorie ajouté", 0),
    CATEGORY_DELETED_SUCSEFULLY("categorie supprimé avec succées",0),
    
    PRODUCT_EXIST("produit trouvé dans Easylink",1),
    TOKEN_NOT_AUTHORIZED("access token not authorized",401),
    TOKEN_MISSING("The Bearer Token is missing",402),
    LIST_PRODUCT_EMPTY("liste produit vide dans Easylink", 0),
    LIST_PRODUCT_NOT_EMPTY("liste produit n'est pas vide dans Easylink", 1),
    LIST_CATEGORY_EMPTY("liste category vide dans Easylink", 0),
    LIST_CATEGORY_NOT_EMPTY("liste category n'est pas vide dans Easylink", 1),
	FAILED_TO_ADD_PRODUCT_CATEGORY_NOT_EXIST("Catégorie non trouvée dans la base de donnée", -1),
	FAILED_TO_ADD_PRODUCT_BRAND_NOT_EXIST("Marque non trouvée dans la base de donnée", -2),
	FAILED_TO_ADD_PRODUCT_VAT_NOT_EXIST("taxe non trouvée dans la base de donnée", -3),
	PRODUCT_DELETED_SUCESSFULLY("produit supprimé avec succés ", 0),
	PRODUCT_VALIDATED_SUCESSFULLY("produit validé avec succés ", 0),
	ADD_PRODUCT_SUCESSFUL("produit ajouté avec succés ", 0),
	OFFER_PRODUCT_FAILED("produit  appartient déja à cette offre  ", -1),
	OFFER_PRODUCT_SUCESSFUL("produit  a été ajoutée à cette offre avec succés ", 0),
	BRAND_ALREADY_EXIST("marque existe déja ", -1),
	BRAND_ADDED_UCCESFULLY("marque ajouté avec Succés ", 0),
	BRAND_DOES_NOT_EXIST("marque inexistante ", -1),
	BRAND_REMOVED_UCCESFULLY("marque supprimée avec Succés ", 0),
	BRAND_UPDATED_UCCESFULLY("marque modifiée avec Succés ", 0),
	PRODUCT_REMOVED_FROM_OFFER("porduit supprimé",0),
	PRODUCT_REMOVED_FROM_OFFER_FAIL("porduit inexistant",-1),
	PRODUCT_UPDATED("produit modifié avec succés",0),
	PRODUCT_UPDATE_FAILED("produit inexistant",-1);


    private static final Map<Integer, EnumMessage> BY_CODE = new HashMap<>();
    private static final Map<String, EnumMessage> BY_LABEL = new HashMap<>();

    static {
        for (EnumMessage e : values()) {
            BY_LABEL.put(e.label, e);
            BY_CODE.put(e.code, e);
        }
    }

    public final String label;
    public final Integer code;

    private EnumMessage(String label, Integer code) {
        this.label = label;
        this.code = code;
    }

    public static EnumMessage valueOfLabel(String label) {
        return BY_LABEL.get(label);
    }

    public static EnumMessage valueOfCode(Integer number) {
        return BY_CODE.get(number);
    }
}
