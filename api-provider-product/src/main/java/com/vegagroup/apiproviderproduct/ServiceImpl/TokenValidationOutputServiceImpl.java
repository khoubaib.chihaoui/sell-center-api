package com.vegagroup.apiproviderproduct.ServiceImpl;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vegagroup.apiproviderproduct.Service.TokenValidationOutputService;
import com.vegagroup.apiproviderproduct.constants.ApiConstants;
import com.vegagroup.apiproviderproduct.dto.TokenValidationOutput;
import com.vegagroup.apiproviderproduct.dto.Enum.EnumMessage;
import com.vegagroup.apiproviderproduct.dto.error.CustomException;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;


@Service
public class TokenValidationOutputServiceImpl implements TokenValidationOutputService {

    Logger logger = LoggerFactory.getLogger(TokenValidationOutputServiceImpl.class);


    @Override
    public TokenValidationOutput getValidate(HttpServletRequest request) {
        logger.info("CustomerEndpoint getValidate Start" );
        String authorization = request.getHeader(ApiConstants.AUTHORIZATION);
        TokenValidationOutput tokenValidationOutput = null;
        Gson gson = new GsonBuilder().create();
        if (authorization != null && authorization.startsWith(ApiConstants.BEARER)) {
            logger.info("CustomerEndpoint getValidate authorization: "+authorization);
            String token = authorization.substring(ApiConstants.BEARER.length()).trim();
            InputStream is = null ;
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            try {
                URL url = new URL(ApiConstants.TOKEN_VALIDATE + token);
                is = url.openStream();
                Reader targetReader = new InputStreamReader(is);
                tokenValidationOutput = gson.fromJson(targetReader, TokenValidationOutput.class);


            } catch (MalformedURLException e) {
                logger.error("CustomerEndpoint getValidate MalformedURLException " +e.getCause() +" "+ e.getMessage()  );
                e.printStackTrace();
                throw new CustomException(e.getMessage(),e.getCause());
            } catch (IOException e) {
                logger.error("CustomerEndpoint getValidate IOException " +e.getCause() +" "+ e.getMessage()  );
                e.printStackTrace();
                throw new CustomException(e.getMessage(),e.getCause());
            } finally {
                try {
                    is.close();
                } catch (IOException e) {
                    logger.error("CustomerEndpoint getValidate is.close IOException " +e.getCause() +" "+ e.getMessage());
                    e.printStackTrace();
                    throw new CustomException(e.getMessage(),e.getCause());
                }
            }

            if(tokenValidationOutput != null) {
                return tokenValidationOutput;

            } else {
                logger.info("CustomerEndpoint getValidate TOKEN NOT AUTHORIZED");
                return new TokenValidationOutput(EnumMessage.TOKEN_NOT_AUTHORIZED.code ,EnumMessage.TOKEN_NOT_AUTHORIZED.label);
            }

        } else {
            logger.info("CustomerEndpoint getValidate TOKEN MISSING");
            return new TokenValidationOutput(EnumMessage.TOKEN_MISSING.code ,EnumMessage.TOKEN_MISSING.label);
        }
    }
}
