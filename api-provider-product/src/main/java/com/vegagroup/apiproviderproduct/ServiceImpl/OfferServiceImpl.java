package com.vegagroup.apiproviderproduct.ServiceImpl;


import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vegagroup.apiproviderproduct.Repository.ConventionRepository;
import com.vegagroup.apiproviderproduct.Repository.OfferRepository;
import com.vegagroup.apiproviderproduct.Repository.ProductRepository;
import com.vegagroup.apiproviderproduct.Service.OfferService;
import com.vegagroup.apiproviderproduct.dto.OfferDto;
import com.vegagroup.apiproviderproduct.dto.Enum.EnumMessage;
import com.vegagroup.apiproviderproduct.dto.Response.OfferResponse;
import com.vegagroup.apiproviderproduct.dto.Response.ProductResponse;
import com.vegagroup.apiproviderproduct.mapper.OfferMapper;

import model.Category;
import model.Convention;
import model.Offer;
import model.Product;

/**
 * Created by ahmed on 18/07/2019.
 */
@Service
public class OfferServiceImpl implements OfferService {

    @Autowired
    private OfferRepository offerRepository;
    
    @Autowired
    private ConventionRepository conventionRepository;
    
    @Autowired
	private ProductRepository productRepository;
    
    @Autowired
    private OfferMapper offermapper;
	@Override
	public OfferResponse AddNewOffer(OfferDto offerdto,Integer ConventionId) {
		// TODO Auto-generated method stub
		
		Offer offer = offermapper.OfferDtoToOffer(offerdto);
		Optional<Convention> opConvention = conventionRepository.findById(ConventionId);
		if(!opConvention.isPresent())
		{
			return new OfferResponse(EnumMessage.CONVENTION_NOT_EXIST.code,
					EnumMessage.CONVENTION_NOT_EXIST.label, null);
		}
		
		offer.setConvention(opConvention.get());
		offerRepository.save(offer);
		return new OfferResponse(0,
				"offre créée avec succés !", null);

	}
	@Override
	public OfferResponse AddProductToOffre(Integer idOffre, Integer IdProduct) {
		Optional<Offer> opOffre = offerRepository.findById(idOffre);
		
		Set<Product> products = opOffre.get().getProducts();
		
		Optional<Product> opProduct = productRepository.findById(IdProduct);
		
		if(products.add(opProduct.get())==false)
		{
			return new OfferResponse(EnumMessage.OFFER_PRODUCT_FAILED.code,
					EnumMessage.OFFER_PRODUCT_FAILED.label, null);

		}else 
			products.add(opProduct.get());
		opOffre.get().setProducts(products);
		offerRepository.save(opOffre.get());
		
		return new OfferResponse(EnumMessage.OFFER_PRODUCT_SUCESSFUL.code,
				EnumMessage.OFFER_PRODUCT_SUCESSFUL.label, null);
}
	@Override
	public OfferResponse RemoveProductFromOffer(Integer idOffre, Integer IdProduct) {
Optional<Offer> opOffre = offerRepository.findById(idOffre);
		
		Set<Product> products = opOffre.get().getProducts();
		
		Optional<Product> opProduct = productRepository.findById(IdProduct);
		
		if(products.remove(opProduct.get())==false)
		{
			return new OfferResponse(EnumMessage.PRODUCT_REMOVED_FROM_OFFER_FAIL.code,
					EnumMessage.PRODUCT_REMOVED_FROM_OFFER_FAIL.label, null);

		}else 
			products.remove((opProduct.get()));
		opOffre.get().setProducts(products);
		offerRepository.save(opOffre.get());
		
		return new OfferResponse(EnumMessage.PRODUCT_REMOVED_FROM_OFFER.code,
				EnumMessage.PRODUCT_REMOVED_FROM_OFFER.label, null);
	}
}
