package com.vegagroup.apiproviderproduct.ServiceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vegagroup.apiproviderproduct.Repository.CategoryRepository;
import com.vegagroup.apiproviderproduct.Repository.ProductRepository;
import com.vegagroup.apiproviderproduct.Service.CategoryService;
import com.vegagroup.apiproviderproduct.dto.CategoryDto;
import com.vegagroup.apiproviderproduct.dto.Enum.EnumMessage;
import com.vegagroup.apiproviderproduct.dto.Response.CategoryResponse;
import com.vegagroup.apiproviderproduct.mapper.CategoryMapper;

import model.Category;
import model.Product;

@Service
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	private CategoryRepository categoryrepository;

	@Autowired
	private CategoryMapper categorymapper;

	@Autowired
	private ProductRepository productrepository;

	@Override
	public CategoryResponse saveCategory(CategoryDto categorydto) {

		Category category = categorymapper.categoryDtoToCategory(categorydto);
		if (!findAllCategory().stream().anyMatch(c -> c.getName().equals(categorydto.getName()))) {
			categoryrepository.save(category);
			return new CategoryResponse(EnumMessage.CATEGORY_ADDED.code, EnumMessage.CATEGORY_ADDED.label, null);
		} else {
			return new CategoryResponse(EnumMessage.CATEGORY_EXIST.code, EnumMessage.CATEGORY_EXIST.label, null);
		}

	}

	@Override
	public CategoryResponse saveSubCategory(int categoryid, CategoryDto categorydto) {
		Optional<Category> opCategorie = categoryrepository.findById(categoryid);
		if (opCategorie.isPresent()) {
			if (!findAllCategory().stream().anyMatch(c -> c.getName().equals(categorydto.getName()))) {
				Category category = new Category();
				category.setName(categorydto.getName());
				category.setCatCategoryid(opCategorie.get());
				categoryrepository.save(category);
				return new CategoryResponse(EnumMessage.SUBCATEGORY_ADDED.code, EnumMessage.SUBCATEGORY_ADDED.label,
						null);
			}
		}
		return new CategoryResponse(EnumMessage.SUBCATEGORY_NOT_EXIST.code, EnumMessage.SUBCATEGORY_NOT_EXIST.label,
				null);

	}

	@Override
	public CategoryResponse deleteCategory(int categoryid) {
		Optional<Category> opCategorie = categoryrepository.findById(categoryid);
		if (opCategorie.isPresent()) {

			categoryrepository.deleteById(categoryid);
			return new CategoryResponse(EnumMessage.CATEGORY_DELETED_SUCSEFULLY.code,
					EnumMessage.CATEGORY_DELETED_SUCSEFULLY.label, null);
		} else {
			return new CategoryResponse(EnumMessage.CATEGORY_NOT_DELETED.code, EnumMessage.CATEGORY_NOT_DELETED.label,
					null);
		}
	}

	@Override
	public List<Category> findAllCategory() {
		List<Category> categories = categoryrepository.findAll();
		return categories;
	}

	@Override
	public CategoryResponse findCategoryByProductId(int idProduit) {
		Optional<Product> opcategorie = productrepository.findById(idProduit);
		if (opcategorie.isPresent()) {

			return new CategoryResponse(EnumMessage.LIST_CATEGORY_NOT_EMPTY.code,
					EnumMessage.LIST_CATEGORY_NOT_EMPTY.label, opcategorie.get().getCategoryid());

		}
		return new CategoryResponse(EnumMessage.LIST_CATEGORY_EMPTY.code,
				EnumMessage.LIST_CATEGORY_EMPTY.label, null);
	}

	@Override
	public CategoryResponse findSubCategoryByCategoryId(int categoryid) {
		Optional<Category> opcategorie = categoryrepository.findById(categoryid);
		if (opcategorie.isPresent()) {

			return new CategoryResponse(EnumMessage.LIST_CATEGORY_NOT_EMPTY.code,
					EnumMessage.LIST_CATEGORY_NOT_EMPTY.label, opcategorie.get().getCategoryid());

		}

		return new CategoryResponse(EnumMessage.LIST_CATEGORY_EMPTY.code, 
				EnumMessage.LIST_CATEGORY_EMPTY.label,opcategorie.get().getCategoryid());
	}

	public CategoryRepository getCategoryrepository() {
		return categoryrepository;
	}

	public void setCategoryrepository(CategoryRepository categoryrepository) {
		this.categoryrepository = categoryrepository;
	}

	public CategoryMapper getCategorymapper() {
		return categorymapper;
	}

	public void setCategorymapper(CategoryMapper categorymapper) {
		this.categorymapper = categorymapper;
	}

}
