package com.vegagroup.apiproviderproduct.ServiceImpl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vegagroup.apiproviderproduct.Repository.AmicaleRepository;
import com.vegagroup.apiproviderproduct.Service.AmicalService;

import model.Amicale;

/**
 * Created by ahmed on 17/07/2019.
 */
@Service
public class AmicaleServiceImpl implements AmicalService {

    @Autowired
    private AmicaleRepository amicaleRepository;

    @Override
    public Amicale findByAmicaleid(Integer amicaleid) {
        return amicaleRepository.findByAmicaleid(amicaleid);
    }
}
