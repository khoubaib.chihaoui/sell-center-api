package com.vegagroup.apiproviderproduct.ServiceImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.query.criteria.internal.compile.CriteriaQueryTypeQueryAdapter;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.vegagroup.apiproviderproduct.Controller.ProductController;
import com.vegagroup.apiproviderproduct.Repository.BrandRepository;
import com.vegagroup.apiproviderproduct.Repository.CategoryRepository;
import com.vegagroup.apiproviderproduct.Repository.ProductCharacteristicRepository;
import com.vegagroup.apiproviderproduct.Repository.CharacteristicRepository;
import com.vegagroup.apiproviderproduct.Repository.ProductRepository;
import com.vegagroup.apiproviderproduct.Repository.UserRepository;
import com.vegagroup.apiproviderproduct.Repository.VatRepository;
import com.vegagroup.apiproviderproduct.Service.ProductService;
import com.vegagroup.apiproviderproduct.dto.ProductDto;
import com.vegagroup.apiproviderproduct.dto.Enum.EnumMessage;
import com.vegagroup.apiproviderproduct.dto.Response.ProductResponse;

import model.Brand;
import model.Category;
import model.Characteristic;
import model.Product;
import model.ProductCharacteristic;
import model.User;
import model.Vat;

/**
 * Created by ahmed on 17/07/2019.
 */
@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductRepository productRepository;
	@Autowired
	private CategoryRepository categoriepository;
	@Autowired
	private BrandRepository brandrepository;
	@Autowired
	private VatRepository vatrepository;
	@Autowired
	private UserRepository userrepository;
	@Autowired
	private CharacteristicRepository CharacteristicRepository;
	@Autowired
	private ProductCharacteristicRepository Prodcaracteristicrepository;
	@PersistenceContext
    private EntityManager entityManager;
	org.slf4j.Logger logger = LoggerFactory.getLogger(ProductController.class);

	@Override
	public ProductResponse AddProduct(ProductDto productdto, Integer idcategorie, Integer brandId, Integer vatId) {
		Optional<Category> categorie = categoriepository.findById(idcategorie);
		Optional<Brand> OpBrand = brandrepository.findById(brandId);
		Optional<Vat> OpVat = vatrepository.findById(vatId);
		;
//		 
		if (!categorie.isPresent()) {
			return new ProductResponse(EnumMessage.FAILED_TO_ADD_PRODUCT_CATEGORY_NOT_EXIST.code,
					EnumMessage.FAILED_TO_ADD_PRODUCT_CATEGORY_NOT_EXIST.label, null);
		}
		if (!OpBrand.isPresent()) {
			return new ProductResponse(EnumMessage.FAILED_TO_ADD_PRODUCT_BRAND_NOT_EXIST.code,
					EnumMessage.FAILED_TO_ADD_PRODUCT_BRAND_NOT_EXIST.label, null);
		}
		if (!OpVat.isPresent()) {
			return new ProductResponse(EnumMessage.FAILED_TO_ADD_PRODUCT_VAT_NOT_EXIST.code,
					EnumMessage.FAILED_TO_ADD_PRODUCT_VAT_NOT_EXIST.label, null);
		}
		if (categorie.isPresent()) {
			Product product = new Product();
			product.setAddeddate(productdto.getAddeddate());
			product.setConventionprice(productdto.getConventionprice());
			product.setDeleteddate(null);
			product.setDeliveryfees(productdto.getDeliveryfees());
			product.setDepth(productdto.getDepth());
			product.setDescription(productdto.getDescription());
			product.setDisplayoutofstock(productdto.getDisplayoutofstock());
			product.setFreedelivery(productdto.getFreedelivery());
			product.setHeight(productdto.getHeight());
			product.setIsactif(productdto.getIsactif());
			product.setIsdeleted(productdto.getIsdeleted());
			product.setIsnew(productdto.getIsnew());
			product.setIspromotion(productdto.getIspromotion());
			product.setLength(productdto.getLength());
			product.setName(productdto.getName());
			product.setPayments(productdto.getPayments());
			product.setPriceht(productdto.getPriceht());
			product.setPricetype(productdto.getPricetype());
			product.setPublicprice(productdto.getPublicprice());
			product.setReference(productdto.getReference());
			product.setRejectioncause(productdto.getRejectioncause());
			product.setSerialized(productdto.getSerialized());
			product.setSku(productdto.getSku());
			product.setSn(productdto.getSn());
			product.setSpecs(productdto.getSpecs());
			product.setSoldstock(productdto.getSoldstock());
			product.setStock(productdto.getStock());
			product.setTerms(productdto.getTerms());
			product.setValidation(null);
			product.setValidationdate(null);
			product.setWaranty(productdto.getWaranty());
			product.setWeight(productdto.getWeight());
			product.setWithdelivery(productdto.getWithdelivery());
			product.setCategoryid(categorie.get());
			product.setBrandid(OpBrand.get());
			product.setVatid(OpVat.get());
			productRepository.save(product);
		}
		return new ProductResponse(EnumMessage.ADD_PRODUCT_SUCESSFUL.code, EnumMessage.ADD_PRODUCT_SUCESSFUL.label,
				productdto);

		// return (int)produit.getIdProduit();

	}

	@Override
	public ProductResponse deleteProduit(Integer produitId) {
		Optional<Product> opProduit=productRepository.findById(produitId);
		if(opProduit.isPresent())
		{
			
		opProduit.get().setIsdeleted((short) 1);
		opProduit.get().setDeleteddate(new Date());
		productRepository.save(opProduit.get());
		
		
		return new ProductResponse(EnumMessage.PRODUCT_DELETED_SUCESSFULLY.code, EnumMessage.PRODUCT_DELETED_SUCESSFULLY.label,
				null);
		
		}
		return new ProductResponse(EnumMessage.PRODUCT_NOT_EXIST.code, EnumMessage.PRODUCT_NOT_EXIST.label,
				null);
		
		
		
	
	}

	@Override
	public ProductResponse ValidProduit(Integer produitId, Integer userId) {
		
		Optional<Product> opProduit=productRepository.findById(produitId);
		Optional<User> opUser=userrepository.findById(userId);
		if(opProduit.isPresent()&&opUser.isPresent())
		{
			
		opProduit.get().setValidation((short) 1);
		opProduit.get().setValidationdate(new Date());
		opProduit.get().setValidationby(opUser.get());
		productRepository.save(opProduit.get());
		
		
		return new ProductResponse(EnumMessage.PRODUCT_VALIDATED_SUCESSFULLY.code, EnumMessage.PRODUCT_VALIDATED_SUCESSFULLY.label,
				null);
		
		}
		return new ProductResponse(EnumMessage.PRODUCT_NOT_EXIST.code, EnumMessage.PRODUCT_NOT_EXIST.label,
				null);
		
	}

	@Override
	public ProductResponse AssignCharacteristicToProduct(Integer idproduct,Integer charcteristiqueid, String Value) {
		// TODO Auto-generated method stub
		Optional<Product> opProduit=productRepository.findById(idproduct);
		Optional<Category> opcategorie=categoriepository.findById(opProduit.get().getCategoryid().getCategoryid());
		List<Characteristic> charactesiques= new ArrayList<Characteristic>(entityManager.createQuery
				("select c from Characteristic c where  c.categoryid.categoryid=:id"
					,Characteristic.class).setParameter("id", opcategorie.get().getCategoryid()).getResultList());
		if(charactesiques.stream(). anyMatch(c->c.getCharid()==charcteristiqueid)==true)
		{
			Optional<Characteristic> characterstic = CharacteristicRepository.findById(charcteristiqueid);
			ProductCharacteristic prodcharacteristic = new ProductCharacteristic();
			prodcharacteristic.setProduct(opProduit.get());
			prodcharacteristic.setCharid(characterstic.get());
			prodcharacteristic.setValue(Value);
			Prodcaracteristicrepository.save(prodcharacteristic);
			return new ProductResponse(0, "",
					null);
			
		}
		
		
		return new ProductResponse(-1, "",
				null);
	}

	@Override
	public ProductResponse UpdateProduct(ProductDto productdto, Integer produitId) {
		
		Optional<Product> opProduit=productRepository.findById(produitId);
		
		if(opProduit.get()!=null) {
		if(productdto.getAddeddate()!=null){
				opProduit.get().setAddeddate(productdto.getAddeddate());
			}
		if(productdto.getConventionprice()!=null){
			opProduit.get().setConventionprice(productdto.getConventionprice());
		}
		if(productdto.getDeleteddate()!=null){
			opProduit.get().setDeleteddate(productdto.getDeleteddate());
		}
		if(productdto.getDeliveryfees()!=0){
			opProduit.get().setDeliveryfees(productdto.getDeliveryfees());
		}
		if(productdto.getDepth()!=0){
			opProduit.get().setDepth(productdto.getDepth());
		}
		if(productdto.getDescription()!=null){
			opProduit.get().setDescription(productdto.getDescription());
		}
		if(productdto.getDisplayoutofstock()!=null){
			opProduit.get().setDisplayoutofstock(productdto.getDisplayoutofstock());
		}
		if(productdto.getFreedelivery()!=null){
			opProduit.get().setFreedelivery(productdto.getFreedelivery());
		}
		if(productdto.getHeight()!=0){
			opProduit.get().setHeight(productdto.getHeight());
		}
		if(productdto.getIsactif()!=null){
			opProduit.get().setIsactif(productdto.getIsactif());
		}
		if(productdto.getIsdeleted()!=null){
			opProduit.get().setIsdeleted(productdto.getIsdeleted());
		}
		if(productdto.getIsnew()!=null){
			opProduit.get().setIsnew(productdto.getIsnew());
		}
		if(productdto.getIspromotion()!=null){
			opProduit.get().setIspromotion(productdto.getIspromotion());
		}
		if(productdto.getLength()!=0){
			opProduit.get().setLength(productdto.getLength());
		}
		if(productdto.getName()!=null){
			opProduit.get().setName(productdto.getName());
		}
		if(productdto.getPayments()!=null){
			opProduit.get().setPayments(productdto.getPayments());
		}
		if(productdto.getPriceht()!=0){
			opProduit.get().setPriceht(productdto.getPriceht());
		}
		if(productdto.getPublicprice()!=0){
			opProduit.get().setPublicprice(productdto.getPublicprice());
		}
		if(productdto.getReference()!=null){
			opProduit.get().setReference(productdto.getReference());
		}
		if(productdto.getRejectioncause()!=null){
			opProduit.get().setRejectioncause(productdto.getRejectioncause());
		}
		if(productdto.getSku()!=null){
			opProduit.get().setSku(productdto.getSku());
		}
		if(productdto.getSn()!=null){
			opProduit.get().setSn(productdto.getSn());
		}
		if(productdto.getSoldstock()!=0){
			opProduit.get().setSoldstock(productdto.getSoldstock());
		}
		if(productdto.getSerialized()!=0){
			opProduit.get().setSerialized(productdto.getSerialized());
		}
		if(productdto.getSpecs()!=null){
			opProduit.get().setSpecs(productdto.getSpecs());
		}
		if(productdto.getStock()!=0){
			opProduit.get().setStock(productdto.getStock());
		}
		if(productdto.getTerms()!=0){
			opProduit.get().setTerms(productdto.getTerms());
		}
		if(productdto.getValidation()!=0){
			opProduit.get().setValidation(productdto.getValidation());
		}
		if(productdto.getValidationdate()!=null){
			opProduit.get().setValidationdate(productdto.getValidationdate());
		}
		if(productdto.getWaranty()!=0){
			opProduit.get().setWaranty(productdto.getWaranty());
		}
		if(productdto.getWeight()!=0){
			opProduit.get().setWeight(productdto.getWeight());
		}
		if(productdto.getWithdelivery()!=0){
			opProduit.get().setWithdelivery(productdto.getWithdelivery());
		}
		
		productRepository.save(opProduit.get());
		
		return new ProductResponse(EnumMessage.PRODUCT_UPDATED.code, EnumMessage.PRODUCT_UPDATED.label, null);
		}
		return new ProductResponse(EnumMessage.PRODUCT_UPDATE_FAILED.code, EnumMessage.PRODUCT_UPDATE_FAILED.label, null);

	}


}
