package com.vegagroup.apiproviderproduct.ServiceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vegagroup.apiproviderproduct.Repository.BrandRepository;
import com.vegagroup.apiproviderproduct.Service.BrandService;
import com.vegagroup.apiproviderproduct.dto.BrandDto;
import com.vegagroup.apiproviderproduct.dto.Enum.EnumMessage;
import com.vegagroup.apiproviderproduct.dto.Response.BrandResponse;
import com.vegagroup.apiproviderproduct.mapper.BrandMapper;

import model.Brand;

@Service
public class BrandServiceImpl implements BrandService{

	@Autowired
	BrandRepository brandrepository;
	
	@Override
	public BrandResponse AddNewBrand(BrandDto brandDto) {
	
		Brand brand = new Brand();
		
		List<Brand> brands = brandrepository.findAll();
		for (Brand marque : brands)
		{
			if(marque.getName().toUpperCase().equals(brandDto.getName().toUpperCase()))
				return new BrandResponse(EnumMessage.BRAND_ALREADY_EXIST.code,EnumMessage.BRAND_ALREADY_EXIST.label,null);
		}
		
		brand.setName(brandDto.getName());
		brand.setPicture(brandDto.getPicture());
		brandrepository.save(brand);
		return new BrandResponse(EnumMessage.BRAND_ADDED_UCCESFULLY.code,EnumMessage.BRAND_ADDED_UCCESFULLY.label,null);

	}
	@Override
	public BrandResponse DeleteBrand(Integer brandId) {
		Optional<Brand>OpBrand=brandrepository.findById(brandId);
		
		if(!OpBrand.isPresent())
		{
			return new BrandResponse(EnumMessage.BRAND_DOES_NOT_EXIST.code,EnumMessage.BRAND_DOES_NOT_EXIST.label,null);

		}
		brandrepository.delete(OpBrand.get());
		return new BrandResponse(EnumMessage.BRAND_REMOVED_UCCESFULLY.code,EnumMessage.BRAND_REMOVED_UCCESFULLY.label,null);

	}
	@Override
	public BrandResponse UpdateBrand(BrandDto brandDto, Integer Brandid) {
		Optional<Brand>OpBrand=brandrepository.findById(Brandid);
		if(!OpBrand.isPresent())
		{
			return new BrandResponse(EnumMessage.BRAND_DOES_NOT_EXIST.code,EnumMessage.BRAND_DOES_NOT_EXIST.label,null);

		}
		if(brandDto.getName()!=null){
			OpBrand.get().setName(brandDto.getName());
		}
		if(brandDto.getPicture()!=null){
			OpBrand.get().setPicture(brandDto.getPicture());
		}
		brandrepository.save(OpBrand.get());
		return new BrandResponse(EnumMessage.BRAND_UPDATED_UCCESFULLY.code,EnumMessage.BRAND_UPDATED_UCCESFULLY.label,null);

	}
	
	

}
