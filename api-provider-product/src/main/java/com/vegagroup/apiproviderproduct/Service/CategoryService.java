package com.vegagroup.apiproviderproduct.Service;


import java.util.List;

import com.vegagroup.apiproviderproduct.dto.CategoryDto;
import com.vegagroup.apiproviderproduct.dto.Response.CategoryResponse;

import model.Category;


public interface CategoryService {
	
	
	public CategoryResponse saveCategory(CategoryDto categorydto);
	
	public CategoryResponse saveSubCategory(int categoryid, CategoryDto categorydto);

	public CategoryResponse deleteCategory(int categoryid);

	public List<Category> findAllCategory();

	public CategoryResponse findCategoryByProductId(int idProduit);

	public CategoryResponse findSubCategoryByCategoryId(int categoryid);


}
