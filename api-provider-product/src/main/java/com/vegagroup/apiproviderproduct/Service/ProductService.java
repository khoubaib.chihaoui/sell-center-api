package com.vegagroup.apiproviderproduct.Service;

import com.vegagroup.apiproviderproduct.dto.ProductDto;
import com.vegagroup.apiproviderproduct.dto.Response.ProductResponse;

public interface ProductService {
	
    public ProductResponse AddProduct(ProductDto produitnotmapped,Integer idcategorie,Integer brandId
			,Integer vatId);
    
	public ProductResponse deleteProduit(Integer produitId);
	
	public ProductResponse ValidProduit(Integer produitId,Integer userId);
	
	public ProductResponse AssignCharacteristicToProduct(Integer idproduct,Integer charcteristiqueid,String Value);

	public ProductResponse UpdateProduct(ProductDto productdto,Integer produitId);
}
