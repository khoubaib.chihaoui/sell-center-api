package com.vegagroup.apiproviderproduct.Service;



import javax.servlet.http.HttpServletRequest;

import com.vegagroup.apiproviderproduct.dto.TokenValidationOutput;


public interface TokenValidationOutputService {
    public TokenValidationOutput getValidate(HttpServletRequest request);
}
