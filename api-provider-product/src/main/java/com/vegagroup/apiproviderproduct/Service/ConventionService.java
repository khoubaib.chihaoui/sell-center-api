package com.vegagroup.apiproviderproduct.Service;

import model.Convention;

/**
 * Created by ahmed on 18/07/2019.
 */
public interface ConventionService {

    public Convention findConventionByConvetionid(Integer ConventionID);
}
