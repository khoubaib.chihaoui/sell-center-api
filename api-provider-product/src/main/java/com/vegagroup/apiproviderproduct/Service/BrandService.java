package com.vegagroup.apiproviderproduct.Service;

import com.vegagroup.apiproviderproduct.dto.BrandDto;
import com.vegagroup.apiproviderproduct.dto.Response.BrandResponse;

public interface BrandService {
	public BrandResponse AddNewBrand(BrandDto brandDto);
	public BrandResponse DeleteBrand(Integer brandId);
	public BrandResponse UpdateBrand(BrandDto brandDto,Integer Brandid);

}
