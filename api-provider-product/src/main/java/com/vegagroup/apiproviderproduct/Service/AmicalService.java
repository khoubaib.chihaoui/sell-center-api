package com.vegagroup.apiproviderproduct.Service;

import model.Amicale;

/**
 * Created by ahmed on 17/07/2019.
 */
public interface AmicalService {

    /**
     * @param amicaleid
     * @return
     */
    public Amicale findByAmicaleid(Integer amicaleid);
}
