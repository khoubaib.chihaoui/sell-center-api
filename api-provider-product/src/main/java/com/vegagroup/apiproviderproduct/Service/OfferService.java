package com.vegagroup.apiproviderproduct.Service;

import com.vegagroup.apiproviderproduct.dto.OfferDto;
import com.vegagroup.apiproviderproduct.dto.Response.OfferResponse;

public interface OfferService {

	public OfferResponse AddNewOffer(OfferDto dto,Integer ConventionId);
	public OfferResponse AddProductToOffre(Integer idOffre,Integer IdProduct);
	public OfferResponse RemoveProductFromOffer(Integer idOffre,Integer IdProduct);
	
}
