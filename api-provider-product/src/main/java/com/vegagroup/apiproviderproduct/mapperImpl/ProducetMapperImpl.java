package com.vegagroup.apiproviderproduct.mapperImpl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vegagroup.apiproviderproduct.dto.ProductDto;
import com.vegagroup.apiproviderproduct.mapper.ProductMapper;

import model.Product;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by ahmed on 18/07/2019.
 */
@Service
public class ProducetMapperImpl implements ProductMapper {
    @Autowired
    private ProductMapper productMapper;

    @Override
    public ProductDto producToProductDto(Product product) {
        ProductDto productDto=new ProductDto();
        productDto.setAddeddate(product.getAddeddate());
        productDto.setConventionprice(product.getConventionprice());
        productDto.setDeleteddate(product.getDeleteddate());
        productDto.setDeliveryfees(product.getDeliveryfees());
        productDto.setDepth(product.getDepth());
        productDto.setDescription(product.getDescription());
        productDto.setDisplayoutofstock(product.getDisplayoutofstock());
        productDto.setFreedelivery(product.getFreedelivery());
        productDto.setHeight(product.getHeight());
        productDto.setIsactif(product.getIsactif());
        productDto.setIsdeleted(product.getIsdeleted());
        productDto.setIsnew(product.getIsnew());
        productDto.setIspromotion(product.getIspromotion());
        productDto.setLength(product.getLength());
        productDto.setName(product.getName());
        productDto.setPayments(product.getPayments());
        productDto.setPriceht(product.getPriceht());
        productDto.setPricetype(product.getPricetype());
        productDto.setReference(product.getReference());
        productDto.setRejectioncause(product.getRejectioncause());
        productDto.setSerialized(product.getSerialized());
        productDto.setSku(product.getSku());
        productDto.setSn(product.getSn());
        productDto.setSpecs(product.getSpecs());
        productDto.setSoldstock(product.getSoldstock());
        productDto.setStock(product.getStock());
        productDto.setTerms(product.getTerms());
        productDto.setValidation(product.getValidation());
        productDto.setValidationdate(product.getValidationdate());
        productDto.setWaranty(product.getWaranty());
        productDto.setWeight(product.getWeight());
        productDto.setWithdelivery(product.getWithdelivery());
        return productDto;
    }

    @Override
    public Product productDtoToProduct(ProductDto productDto) {
        return null;
    }

    @Override
    public Set<ProductDto> productSetToProductDtoSet(Set<Product> products) {
        Set<ProductDto> productDtos=new HashSet<>();
        products.forEach(product -> {
        	ProductDto productDto=new ProductDto();
            productDto.setAddeddate(product.getAddeddate());
            productDto.setConventionprice(product.getConventionprice());
            productDto.setDeleteddate(product.getDeleteddate());
            productDto.setDeliveryfees(product.getDeliveryfees());
            productDto.setDepth(product.getDepth());
            productDto.setDescription(product.getDescription());
            productDto.setDisplayoutofstock(product.getDisplayoutofstock());
            productDto.setFreedelivery(product.getFreedelivery());
            productDto.setHeight(product.getHeight());
            productDto.setIsactif(product.getIsactif());
            productDto.setIsdeleted(product.getIsdeleted());
            productDto.setIsnew(product.getIsnew());
            productDto.setIspromotion(product.getIspromotion());
            productDto.setLength(product.getLength());
            productDto.setName(product.getName());
            productDto.setPayments(product.getPayments());
            productDto.setPriceht(product.getPriceht());
            productDto.setPricetype(product.getPricetype());
            productDto.setReference(product.getReference());
            productDto.setRejectioncause(product.getRejectioncause());
            productDto.setSerialized(product.getSerialized());
            productDto.setSku(product.getSku());
            productDto.setSn(product.getSn());
            productDto.setSpecs(product.getSpecs());
            productDto.setSoldstock(product.getSoldstock());
            productDto.setStock(product.getStock());
            productDto.setTerms(product.getTerms());
            productDto.setValidation(product.getValidation());
            productDto.setValidationdate(product.getValidationdate());
            productDto.setWaranty(product.getWaranty());
            productDto.setWeight(product.getWeight());
            productDto.setWithdelivery(product.getWithdelivery());
            productDtos.add(productDto);
        });
        return productDtos;
    }

    @Override
    public Set<Product> productDtoSetToProductSet(Set<ProductDto> productDtos) {
        return null;
    }

}
