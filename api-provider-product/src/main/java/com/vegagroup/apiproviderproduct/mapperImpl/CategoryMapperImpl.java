package com.vegagroup.apiproviderproduct.mapperImpl;

import model.Category;
import org.springframework.stereotype.Service;
import com.vegagroup.apiproviderproduct.dto.CategoryDto;
import com.vegagroup.apiproviderproduct.mapper.CategoryMapper;

import java.util.ArrayList;
import java.util.List;


@Service
public class CategoryMapperImpl implements CategoryMapper {
    @Override
	public CategoryDto categoryToCategoryDto(Category category) {
        if(category.getCatCategoryid()!=null){
            return new CategoryDto(category.getCategoryid(),category.getPicture(),category.getIsdeleted(),category.getDeleteddate(),category.getAddeddate(),categoryToCategoryDto(category.getCatCategoryid()),category.getName(),category.getIsactif());
        }
        else{
            return new CategoryDto(category.getCategoryid(),category.getPicture(),category.getIsdeleted(),category.getDeleteddate(),category.getAddeddate(),null,category.getName(),category.getIsactif());
        }

    }

    @Override
    public List<CategoryDto> listcategoryTolistCategoryDto(List<Category> categories) {
        List<CategoryDto>categoryDtos=new ArrayList<>();
        categories.forEach(category -> {
            categoryDtos.add(categoryToCategoryDto(category));
        });
        return null;
    }

	@Override
	public Category categoryDtoToCategory(CategoryDto categorydto) {
		
		return new Category(categorydto.getCategoryid(),categorydto.getPicture(),categorydto.getIsdeleted(),categorydto.getDeleteddate(),categorydto.getAddeddate(),categorydto.getName(),categorydto.getIsactif());
	}
}
