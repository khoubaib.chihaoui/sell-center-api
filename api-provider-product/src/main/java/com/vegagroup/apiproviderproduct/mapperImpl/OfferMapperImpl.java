package com.vegagroup.apiproviderproduct.mapperImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.vegagroup.apiproviderproduct.dto.OfferDto;
import com.vegagroup.apiproviderproduct.mapper.OfferMapper;

import model.Offer;
@Service
public class OfferMapperImpl implements OfferMapper{

	@Override
	public OfferDto offerToOfferDto(Offer offer) {
		// TODO Auto-generated method stub
	return	new OfferDto(offer.getOfferid(),
				offer.getTitle(),
				offer.getDescription(),
				offer.getDiscount(),
				offer.getTranchepayment(),
				offer.getCashpayment(),
				offer.getTypediscount(),
				offer.getTypedeadline(),
				offer.getData(),
				offer.getPriceoffer(),
				offer.getNbrperson(),
				offer.getNbrfamily(),
				offer.getPoster(),
				offer.getIsactif(),
				offer.isIsDeleted(),
				offer.isHided(),
				offer.getMaxoffer(),
				offer.getOffer_id(),
				offer.getService_id(),
				offer.getWithoutautovalidation(),
				offer.getWithdelivery(),
				offer.getSupportnum(),
				offer.getPayondelivery(),
				offer.getDeliveryamount(),
				offer.getWithreservation());
		
	}

	@Override
	public List<OfferDto> listOfferTolistOfferDto(List<Offer> offers) {
		// TODO Auto-generated method stub
		List<OfferDto> offersdto = new ArrayList<OfferDto>();
		offers.stream().forEach(o->{
			offersdto.add(offerToOfferDto(o));
		});
		return offersdto;
	}

	@Override
	public Offer OfferDtoToOffer(OfferDto offerdto) {
		Offer offer = new Offer();
		offer.setCashpayment(offerdto.getCashpayment());
		offer.setData(offerdto.getData());
		offer.setDeliveryamount(offerdto.getDeliveryamount());
		offer.setDescription(offerdto.getDescription());
		offer.setDiscount(offerdto.getDiscount());
		offer.setHided(offerdto.getHided());
		offer.setIsactif(offerdto.getIsactif());
		offer.setIsDeleted(offerdto.getIsDeleted());
		offer.setMaxoffer(offerdto.getMaxoffer());
		offer.setNbrfamily(offerdto.getNbrfamily());
		offer.setNbrperson(offerdto.getNbrfamily());
		offer.setOffer_id(offerdto.getOfferidstring());
		offer.setPayondelivery(offerdto.getPayondelivery());
		offer.setPoster(offerdto.getPoster());
		offer.setPriceoffer(offerdto.getPriceoffer());
		offer.setService_id(offerdto.getService_id());
		offer.setSupportnum(offerdto.getSupportnum());
		offer.setTitle(offerdto.getTitle());
		offer.setTranchepayment(offerdto.getTranchepayment());
		offer.setTypedeadline(offerdto.getTypedeadline());
		offer.setTypediscount(offerdto.getTypediscount());
		offer.setWithdelivery(offerdto.getWithdelivery());
		offer.setWithoutautovalidation(offerdto.getWithoutautovalidation());
		offer.setWithreservation(offerdto.getWithreservation());
		return offer;
	}
}
