package com.vegagroup.apiproviderproduct.mapperImpl;

import com.vegagroup.apiproviderproduct.dto.BrandDto;
import com.vegagroup.apiproviderproduct.mapper.BrandMapper;

import model.Brand;

public class BrandMapperImpl implements BrandMapper{

	@Override
	public BrandDto BrandtoBrandDto(Brand brand) {
		BrandDto brandDto= new BrandDto(brand.getName(),brand.getPicture());
		return brandDto;
		
	}

	@Override
	public Brand BrandDtoToBrand(BrandDto brandDto) {
		// TODO Auto-generated method stub
		return null;
	}

}
