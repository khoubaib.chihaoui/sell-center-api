package com.vegagroup.apiproviderproduct.Controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.vegagroup.apiproviderproduct.Service.ProductService;
import com.vegagroup.apiproviderproduct.dto.ProductDto;
import com.vegagroup.apiproviderproduct.dto.error.CustomException;
import com.vegagroup.apiproviderproduct.mapper.ProductMapper;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;

/**
 * Created by ahmed on 17/07/2019.
 */
@RestController
public class ProductController {
	Logger logger = LoggerFactory.getLogger(ProductController.class);

	@Autowired
	private ProductService productService;

	@Autowired
	private ProductMapper productMapper;

	@PostMapping("/v1/provider/product/addproduct")
	 @ApiOperation(
           value = "Ajouter un produit",
           notes = "Ajout de produit.\n"
                  
                   + "\n<b>result = 0 :</b> Produit Ajouté \n"
                   +"\n<b>result = -1 :</b> Categorie non trouvée \n"
                   + "\n<b>result = -2 :</b> Marque non trouvée \n"
                   + "\n<b>result = -3 :</b> Taxe non trouvée \n"
                   + "\n<b>result = 401 :</b> TOKEN NOT AUTHORIZED\n"
                   + "\n<b>result = 402 :</b> TOKEN MISSING.",
           authorizations = {@Authorization(value = "Bearer")},
           response = Object.class)
   @ApiResponses(value = {
           @ApiResponse(code = 200, message = "OK", response = Object.class),
           @ApiResponse(code = 401, message = "Unauthorized"),
           @ApiResponse(code = 403, message = "Forbidden"),
           @ApiResponse(code = 404, message = "not found")})
	public Object saveProduct(@Valid @RequestBody ProductDto produitnotmapped, Integer idcategorie, Integer brandId,
			Integer vatId) {

		try {
			return productService.AddProduct(produitnotmapped, idcategorie, brandId, vatId);
		} catch (Exception e) {
			logger.error("CustomerEndpoint.getcustomer Exception: " + e.getCause() + " " + e.getMessage());
			return new CustomException(e.getMessage(), e.getCause());
		}

	}

	@PostMapping("/v1/provider/removeproduct/{produitId}")
	@ApiOperation(
	           value = "Supprimer un produit",
	           notes = "Suppression de produit.\n"
	                  
	                   + "\n<b>result = 0 :</b> Produit supprimé \n"
	                   +"\n<b>result = -1 :</b> Produit non trouvée \n"
	        	       + "\n<b>result = 401 :</b> TOKEN NOT AUTHORIZED\n"
	                   + "\n<b>result = 402 :</b> TOKEN MISSING.",
	           authorizations = {@Authorization(value = "Bearer")},
	           response = Object.class)
	   @ApiResponses(value = {
	           @ApiResponse(code = 200, message = "OK", response = Object.class),
	           @ApiResponse(code = 401, message = "Unauthorized"),
	           @ApiResponse(code = 403, message = "Forbidden"),
	           @ApiResponse(code = 404, message = "not found")})
	public Object deleteProduct(@PathVariable(name = "produitId") int produitId) {
		try {
			return productService.deleteProduit(produitId);
		} catch (Exception e) {
			logger.error("CustomerEndpoint.getcustomer Exception: " + e.getCause() + " " + e.getMessage());
			return new CustomException(e.getMessage(), e.getCause());
		}
	}

	@PostMapping("/v1/provider/validateproduct/{produitId}")
	@ApiOperation(
	           value = "Validez un produit",
	           notes = "Validation de produit.\n"
	                  
	                   + "\n<b>result = 0 :</b> Produit Validé \n"
	                   +"\n<b>result = -1 :</b> Produit non trouvée \n"
	        	       + "\n<b>result = 401 :</b> TOKEN NOT AUTHORIZED\n"
	                   + "\n<b>result = 402 :</b> TOKEN MISSING.",
	           authorizations = {@Authorization(value = "Bearer")},
	           response = Object.class)
	   @ApiResponses(value = {
	           @ApiResponse(code = 200, message = "OK", response = Object.class),
	           @ApiResponse(code = 401, message = "Unauthorized"),
	           @ApiResponse(code = 403, message = "Forbidden"),
	           @ApiResponse(code = 404, message = "not found")})

	public Object ValidateProduct(@PathVariable(name = "produitId") Integer produitId, Integer userId) {
		try {
			return productService.ValidProduit(produitId, userId);

		} catch (Exception e) {
			logger.error("CustomerEndpoint.getcustomer Exception: " + e.getCause() + " " + e.getMessage());
			return new CustomException(e.getMessage(), e.getCause());
		}

	}
	@PostMapping("/v1/provider/AddCharToProduct")
	public Object saveCharacteriticTOProduct(Integer idproduct,Integer charcteristiqueid, String Value) {
		try {
			return productService.AssignCharacteristicToProduct(idproduct, charcteristiqueid, Value);

		} catch (Exception e) {
			logger.error("CustomerEndpoint.getcustomer Exception: " + e.getCause() + " " + e.getMessage());
			return new CustomException(e.getMessage(), e.getCause());
		}

	}
	
	

	@PostMapping("/v1/provider/Updateproduct/{produitId}")
	@ApiOperation(
			value = "Modifier un produit",
	          notes = "Modification de produit.\n"
	                 
	                  + "\n<b>result = 0 :</b> Produit Modifié avec succés \n"
	                  +"\n<b>result = -1 :</b> Produit non trouvée \n"
	                  + "\n<b>result = 401 :</b> TOKEN NOT AUTHORIZED\n"
	                  + "\n<b>result = 402 :</b> TOKEN MISSING.",
	           authorizations = {@Authorization(value = "Bearer")},
	           response = Object.class)
	   @ApiResponses(value = {
	           @ApiResponse(code = 200, message = "OK", response = Object.class),
	           @ApiResponse(code = 401, message = "Unauthorized"),
	           @ApiResponse(code = 403, message = "Forbidden"),
	           @ApiResponse(code = 404, message = "not found")})

	public Object UpdateProduct(@RequestBody ProductDto productdto, @PathVariable(name = "produitId") Integer produitId) {
		try {
			return productService.UpdateProduct(productdto, produitId);

		} catch (Exception e) {
			logger.error("CustomerEndpoint.getcustomer Exception: " + e.getCause() + " " + e.getMessage());
			return new CustomException(e.getMessage(), e.getCause());
		}

	}

//      @PostMapping("/v1/GetListProductByAmicale/{amicalID}")
//      @ApiOperation(
//               value = "Afficher la liste des produits d’une amicale donnée",
//               notes = "Return List de produit.\n"
//                       + "\n<b>result = 1 :</b> List produit n’est pas vide</b> \n"
//                       + "\n<b>result = 0 :</b> List produit vide\n"
//                       + "\n<b>result = -1 :</b> amical non trouvée \n"
//                       + "\n<b>result = 401 :</b> TOKEN NOT AUTHORIZED\n"
//                       + "\n<b>result = 402 :</b> TOKEN MISSING.",
//               authorizations = {@Authorization(value = "Bearer")},
//               response = Object.class)
//       @ApiResponses(value = {
//               @ApiResponse(code = 200, message = "OK", response = Object.class),
//               @ApiResponse(code = 401, message = "Unauthorized"),
//               @ApiResponse(code = 403, message = "Forbidden"),
//               @ApiResponse(code = 404, message = "not found")})
//       public Object GetListProductByAmicale(HttpServletRequest request, @PathVariable("amicalID") Integer amicalID){
//          logger.info("ProductController GetListProductByAmicale Start : amicalID "+ amicalID );
//
//          TokenValidationOutput res = tokenValidationOutputService.getValidate(request);
//          try {
//              if(res.getResult()== ApiConstants.TOKEN_VALID) {
//                  logger.info("CustomerEndpoint.getcustomer TOKEN_VALID");
//                  ProductResponse products=productService.GetListProduct(amicalID);
//          return products;
//
//              } else {
//                  logger.info("CustomerEndpoint.getcustomer TOKEN MISSING");
//                  return res;
//              }
//          }catch(Exception e) {
//              logger.error("CustomerEndpoint.getcustomer Exception: " + e.getCause() +" "+ e.getMessage());
//              return new CustomException(e.getMessage(),e.getCause());
//          }
//
//       }
//
//       @PostMapping("/v1/GetProductCharacteristic/{productID}")
//       @ApiOperation(
//               value = "Afficher les caractéristiques d’un produit",
//               notes = "Return les caractéristiques d’un produit.\n"
//                       + "\n<b>result = 1 :</b>produit existant</b> \n"
//                       + "\n<b>result = -1 :</b> Product non trouvée \n"
//                       + "\n<b>result = 401 :</b> TOKEN NOT AUTHORIZED\n"
//                       + "\n<b>result = 402 :</b> TOKEN MISSING.",
//               authorizations = {@Authorization(value = "Bearer")},
//               response = Object.class)
//       @ApiResponses(value = {
//               @ApiResponse(code = 200, message = "OK", response = Object.class),
//               @ApiResponse(code = 401, message = "Unauthorized"),
//               @ApiResponse(code = 403, message = "Forbidden"),
//               @ApiResponse(code = 404, message = "not found")})
//       public Object GetProductCharacteristic(HttpServletRequest request,@PathVariable("productID") Integer productID){
//           logger.info("ProductController GetProductCharacteristic Start : productID "+ productID );
//           TokenValidationOutput res = tokenValidationOutputService.getValidate(request);
//           try {
//               if(res.getResult()== ApiConstants.TOKEN_VALID) {
//                   logger.info("CustomerEndpoint.getcustomer TOKEN VALID" );
//                   ProductResponse product=productService.GetProductCharacteristic(productID);
//               logger.info("product.getResult" + product.getResult() );
//
//               if(product.getResult()==1){
//                       ProductDto productDto= productMapper.producToProductDto((Product) product.getProductInfo());
//                       product.setProductInfo(productDto);
//                       return product;
//                   }else{
//                       return product;
//                   }
//
//               } else {
//                   logger.info("CustomerEndpoint.getcustomer TOKEN MISSING");
//                   return res;
//               }
//           }catch(Exception e) {
//               logger.error("CustomerEndpoint.getcustomer Exception: " + e.getCause() +" "+ e.getMessage());
//               return new CustomException(e.getMessage(),e.getCause());
//           }
//       }
//
//    @PostMapping("/v1/GetProductByOffer/{offerID}")
//    @ApiOperation(
//            value = "Afficher les produits d'une offre donnée",
//            notes = "Return les produits d'une offre donnée.\n"
//                    + "\n<b>result = 1 :</b> List produit n’est pas vide</b> \n"
//                    + "\n<b>result = 0 :</b> List produit vide\n"
//                    + "\n<b>result = -1 :</b> offre non trouvée \n"
//                    + "\n<b>result = 401 :</b> TOKEN NOT AUTHORIZED\n"
//                    + "\n<b>result = 402 :</b> TOKEN MISSING.",
//            authorizations = {@Authorization(value = "Bearer")},
//            response = Object.class)
//    @ApiResponses(value = {
//            @ApiResponse(code = 200, message = "OK", response = Object.class),
//            @ApiResponse(code = 401, message = "Unauthorized"),
//            @ApiResponse(code = 403, message = "Forbidden"),
//            @ApiResponse(code = 404, message = "not found")})
//       public Object GetProductByOffer(HttpServletRequest request,@PathVariable("offerID") Integer offerID){
//        logger.info("ProductController GetProductByOffer Start : offerID "+ offerID );
//        TokenValidationOutput res = tokenValidationOutputService.getValidate(request);
//        try {
//            if(res.getResult()== ApiConstants.TOKEN_VALID) {
//                logger.info("CustomerEndpoint.getcustomer TOKEN VALID" );
//                ProductResponse products=productService.GetProductByOffer(offerID);
//                return products;
//
//            } else {
//                logger.info("CustomerEndpoint.getcustomer TOKEN MISSING");
//                return res;
//            }
//        }catch(Exception e) {
//            logger.error("CustomerEndpoint.getcustomer Exception: " + e.getCause() +" "+ e.getMessage());
//            return new CustomException(e.getMessage(),e.getCause());
//        }
//
//       }
//
//
//       @PostMapping("/v1/GetListProductByConvention/{conventionID}")
//       @ApiOperation(
//               value = "Afficher les produits d'une Convention donnée",
//               notes = "Return les produits d'une Convention donnée.\n"
//                       + "\n<b>result = 1 :</b> List produit n’est pas vide</b> \n"
//                       + "\n<b>result = 0 :</b> List produit vide\n"
//                       + "\n<b>result = -1 :</b> Convention non trouvée \n"
//                       + "\n<b>result = 401 :</b> TOKEN NOT AUTHORIZED\n"
//                       + "\n<b>result = 402 :</b> TOKEN MISSING.",
//               authorizations = {@Authorization(value = "Bearer")},
//               response = Object.class)
//       @ApiResponses(value = {
//               @ApiResponse(code = 200, message = "OK", response = Object.class),
//               @ApiResponse(code = 401, message = "Unauthorized"),
//               @ApiResponse(code = 403, message = "Forbidden"),
//               @ApiResponse(code = 404, message = "not found")})
//       public Object GetProductByConvention(HttpServletRequest request,@PathVariable("conventionID") Integer conventionID){
//           logger.info("ProductController GetProductByConvention Start : conventionID "+ conventionID );
//           TokenValidationOutput res = tokenValidationOutputService.getValidate(request);
//           try {
//               if(res.getResult()== ApiConstants.TOKEN_VALID) {
//                   logger.info("CustomerEndpoint.getcustomer TOKEN VALID" );
//                   ProductResponse products=productService.GetProductByConvention(conventionID);
//               return products;
//
//               } else {
//                   logger.info("CustomerEndpoint.getcustomer TOKEN MISSING");
//                   return res;
//               }
//           }catch(Exception e) {
//               logger.error("CustomerEndpoint.getcustomer Exception: " + e.getCause() +" "+ e.getMessage());
//               return new CustomException(e.getMessage(),e.getCause());
//           }
//
//
//       }
//
//    @PostMapping("/v1/GetListProductBycategory/{categoryID}")
//    @ApiOperation(
//            value = "Afficher les produits d'une catégorie donnée",
//            notes = "Return les produits d'une catégorie donnée.\n"
//                    + "\n<b>result = 1 :</b> List produit n’est pas vide</b> \n"
//                    + "\n<b>result = 0 :</b> List produit vide\n"
//                    + "\n<b>result = -1 :</b> catégorie non trouvée \n"
//                    + "\n<b>result = 401 :</b> TOKEN NOT AUTHORIZED\n"
//                    + "\n<b>result = 402 :</b> TOKEN MISSING.",
//            authorizations = {@Authorization(value = "Bearer")},
//            response = Object.class)
//    @ApiResponses(value = {
//            @ApiResponse(code = 200, message = "OK", response = Object.class),
//            @ApiResponse(code = 401, message = "Unauthorized"),
//            @ApiResponse(code = 403, message = "Forbidden"),
//            @ApiResponse(code = 404, message = "not found")})
//    public Object GetListProductBycategory(HttpServletRequest request,@PathVariable("categoryID") Integer categoryID){
//        logger.info("ProductController GetListProductBycategory Start : categoryID "+ categoryID );
//        TokenValidationOutput res = tokenValidationOutputService.getValidate(request);
//        try {
//            if(res.getResult()== ApiConstants.TOKEN_VALID) {
//                logger.info("CustomerEndpoint.getcustomer TOKEN VALID" );
//                ProductResponse products=productService.GetListProductByCategory(categoryID);
//            return products;
//
//            } else {
//                logger.info("CustomerEndpoint.getcustomer TOKEN MISSING");
//                return res;
//            }
//        }catch(Exception e) {
//            logger.error("CustomerEndpoint.getcustomer Exception: " + e.getCause() +" "+ e.getMessage());
//            return new CustomException(e.getMessage(),e.getCause());
//        }
//    }
//
//    @PostMapping("/v1/GetListNewProductByAmical/{amicalID}")
//    @ApiOperation(
//            value = "Afficher la liste des nouveaux produits d’une amicale donnée",
//            notes = "Return List des nouveaux produit.\n"
//                    + "\n<b>result = 1 :</b> List nouveaux produit n’est pas vide</b> \n"
//                    + "\n<b>result = 0 :</b> List nouveaux produit vide\n"
//                    + "\n<b>result = -1 :</b> amical non trouvée \n"
//                    + "\n<b>result = 401 :</b> TOKEN NOT AUTHORIZED\n"
//                    + "\n<b>result = 402 :</b> TOKEN MISSING.",
//            authorizations = {@Authorization(value = "Bearer")},
//            response = Object.class)
//    @ApiResponses(value = {
//            @ApiResponse(code = 200, message = "OK", response = Object.class),
//            @ApiResponse(code = 401, message = "Unauthorized"),
//            @ApiResponse(code = 403, message = "Forbidden"),
//            @ApiResponse(code = 404, message = "not found")})
//    public Object GetListNewProductByAmical(HttpServletRequest request, @PathVariable("amicalID") Integer amicalID){
//        logger.info("ProductController GetListProductByAmicale Start : amicalID "+ amicalID );
//
//        TokenValidationOutput res = tokenValidationOutputService.getValidate(request);
//        try {
//              //if(res.getResult()== ApiConstants.TOKEN_VALID) {
//            logger.info("CustomerEndpoint.getcustomer TOKEN_VALID");
//            ProductResponse products=productService.GetListNewProductByAmical(amicalID);
//            return products;
//
//              /*} else {
//                  logger.info("CustomerEndpoint.getcustomer TOKEN MISSING");
//                  return res;
//              }*/
//        }catch(Exception e) {
//            logger.error("CustomerEndpoint.getcustomer Exception: " + e.getCause() +" "+ e.getMessage());
//            return new CustomException(e.getMessage(),e.getCause());
//        }
//
//    }
// 

}
