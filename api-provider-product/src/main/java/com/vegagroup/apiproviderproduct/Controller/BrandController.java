package com.vegagroup.apiproviderproduct.Controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.vegagroup.apiproviderproduct.Service.BrandService;
import com.vegagroup.apiproviderproduct.dto.BrandDto;
import com.vegagroup.apiproviderproduct.dto.ProductDto;
import com.vegagroup.apiproviderproduct.dto.error.CustomException;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;

@RestController
public class BrandController {

	@Autowired
	BrandService brandservice;
	
	Logger logger = LoggerFactory.getLogger(BrandController.class);

	@PostMapping("/v1/provider/product/addBrand")
	 @ApiOperation(
          value = "Ajouter une marque",
          notes = "Ajout de marque.\n"
                 
                  + "\n<b>result = 0 :</b> marque ajouté \n"
                  +"\n<b>result = -1 :</b> marque existe déja \n"
                  + "\n<b>result = 401 :</b> TOKEN NOT AUTHORIZED\n"
                  + "\n<b>result = 402 :</b> TOKEN MISSING.",
          authorizations = {@Authorization(value = "Bearer")},
          response = Object.class)
  @ApiResponses(value = {
          @ApiResponse(code = 200, message = "OK", response = Object.class),
          @ApiResponse(code = 401, message = "Unauthorized"),
          @ApiResponse(code = 403, message = "Forbidden"),
          @ApiResponse(code = 404, message = "not found")})
	public Object saveBrand(@Valid @RequestBody BrandDto brandDto) {

		try {
			return brandservice.AddNewBrand(brandDto);
		} catch (Exception e) {
			logger.error("CustomerEndpoint.getcustomer Exception: " + e.getCause() + " " + e.getMessage());
			return new CustomException(e.getMessage(), e.getCause());
		}

	}
	
	@DeleteMapping("/v1/provider/product/deleteBrand/{Brandid}")
	 @ApiOperation(
         value = "supprimer une marque",
         notes = "supperssion de marque.\n"
                
                 + "\n<b>result = 0 :</b> marque supprimé \n"
                 +"\n<b>result = -1 :</b> marque n'esxite pas \n"
                 + "\n<b>result = 401 :</b> TOKEN NOT AUTHORIZED\n"
                 + "\n<b>result = 402 :</b> TOKEN MISSING.",
         authorizations = {@Authorization(value = "Bearer")},
         response = Object.class)
 @ApiResponses(value = {
         @ApiResponse(code = 200, message = "OK", response = Object.class),
         @ApiResponse(code = 401, message = "Unauthorized"),
         @ApiResponse(code = 403, message = "Forbidden"),
         @ApiResponse(code = 404, message = "not found")})
	public Object DeleteBrand(@PathVariable(name = "Brandid")Integer Brandid) {

		try {
			return brandservice.DeleteBrand(Brandid);
		} catch (Exception e) {
			logger.error("CustomerEndpoint.getcustomer Exception: " + e.getCause() + " " + e.getMessage());
			return new CustomException(e.getMessage(), e.getCause());
		}

	}
	
	@PostMapping("/v1/provider/Updatebrand/{Brandid}")
	@ApiOperation(
			value = "Modifier une marque",
	          notes = "Modification de marque.\n"
	                 
	                  + "\n<b>result = 0 :</b> marque Modifiée avec succés \n"
	                  +"\n<b>result = -1 :</b> marque non trouvée \n"
	                  + "\n<b>result = 401 :</b> TOKEN NOT AUTHORIZED\n"
	                  + "\n<b>result = 402 :</b> TOKEN MISSING.",
	           authorizations = {@Authorization(value = "Bearer")},
	           response = Object.class)
	   @ApiResponses(value = {
	           @ApiResponse(code = 200, message = "OK", response = Object.class),
	           @ApiResponse(code = 401, message = "Unauthorized"),
	           @ApiResponse(code = 403, message = "Forbidden"),
	           @ApiResponse(code = 404, message = "not found")})

	public Object UpdateProduct(@PathVariable(name = "Brandid")BrandDto brandDto, Integer Brandid) {
		try {
			return brandservice.UpdateBrand(brandDto, Brandid);

		} catch (Exception e) {
			logger.error("CustomerEndpoint.getcustomer Exception: " + e.getCause() + " " + e.getMessage());
			return new CustomException(e.getMessage(), e.getCause());
		}

	}

}
