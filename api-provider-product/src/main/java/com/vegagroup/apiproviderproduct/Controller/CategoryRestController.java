package com.vegagroup.apiproviderproduct.Controller;

import java.util.List;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vegagroup.apiproviderproduct.Service.CategoryService;
import com.vegagroup.apiproviderproduct.dto.CategoryDto;
import com.vegagroup.apiproviderproduct.dto.Response.CategoryResponse;
import com.vegagroup.apiproviderproduct.dto.error.CustomException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import model.Category;

@RestController
@Api(value = "Category")
@Validated
public class CategoryRestController {

	Logger logger = LoggerFactory.getLogger(CategoryRestController.class);
	@Autowired
	private CategoryService categoryservice;
	//@Autowired
	//private ProductService productService;

	@ApiOperation(value = "Delete Category")
	@ApiResponses(value = { @ApiResponse(code = 0, message = "Suppression avec succées de la categorie "),
			                @ApiResponse(code = -1, message = "Erreur du suppression "), })
	@DeleteMapping("/api/provider/product/category/{categorieId}")
	public Object deleteCategory(@PathVariable(name = "categoryid") int categoryid) {

		return categoryservice.deleteCategory(categoryid);

	}

	@ApiOperation(value = "Add Category")
	@ApiResponses(value = { @ApiResponse(code = 0, message = "Ajout avec succées de la categorie "),
			                @ApiResponse(code = -1, message = "Erreur d'ajout "), })
	@PostMapping("/api/provider/product/category")
	public Object saveCategory(CategoryDto categorydto) {

		try {
			return categoryservice.saveCategory(categorydto);
		} catch (Exception e) {
			logger.error("CustomerEndpoint.getcustomer Exception: " + e.getCause() + " " + e.getMessage());
			return new CustomException(e.getMessage(), e.getCause());
		}

	}

	//// spec manquant/////////////////////////////////*************///////
	@ApiOperation(value = "Add sous category")
	@ApiResponses(value = { @ApiResponse(code = 0, message = "Ajout avec succées de la sous categorie "),
			@ApiResponse(code = -1, message = "Erreur d'ajout "),
			@ApiResponse(code = -2, message = "Categorie n'existe pas"), })
	@PostMapping("/api/provider/product/category/subcategory")
	public Object saveSubCategory(int categoryid, @Valid @RequestBody CategoryDto categorydto) {


		try {
			return categoryservice.saveSubCategory(categoryid, categorydto);
		} catch (Exception e) {
			logger.error("CustomerEndpoint.getcustomer Exception: " + e.getCause() + " " + e.getMessage());
			return new CustomException(e.getMessage(), e.getCause());
		}

	}

	@ApiOperation(value = "Poster all the category")
	@GetMapping("/api/provider/product/category")
	public List<Category> findAllCategory() {
		
		try {
			return categoryservice.findAllCategory();
		} catch (Exception e) {
			logger.error("CustomerEndpoint.getcustomer Exception: " + e.getCause() + " " + e.getMessage());
			return (List<Category>) new CustomException(e.getMessage(), e.getCause());
		}
	}

	@ApiOperation(value = "Find subcategory by categoryId")
	@GetMapping("/api/provider/product/category/GetSubCategoryByCategoryId/{idcategorie}")
	public Object findSubCategoryByCategoryId(@PathVariable(name = "categoryid") int categoryid) {

		try {
			return categoryservice.findSubCategoryByCategoryId(categoryid);
		} catch (Exception e) {
			logger.error("CustomerEndpoint.getcustomer Exception: " + e.getCause() + " " + e.getMessage());
			return new CustomException(e.getMessage(), e.getCause());
		}
	}

	@ApiOperation(value = "Poster category by product")
	@GetMapping("api/provider/product/category/GetCategoryByProduct/{idproduct}")
	public Object findCategoryByProductId(@PathVariable(name = "idproduct") int idProduit) {
		return categoryservice.findCategoryByProductId(idProduit);

	}

	public CategoryService getCategoryservice() {
		return categoryservice;
	}

	public void setCategoryservice(CategoryService categoryservice) {
		this.categoryservice = categoryservice;
	}

	/*public ProductService getProductService() {
		return productService;
	}

	public void setProductService(ProductService productService) {
		this.productService = productService;
	}*/


}
