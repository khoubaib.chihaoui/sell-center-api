package com.vegagroup.apiproviderproduct.Controller;

import javax.validation.Valid;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.vegagroup.apiproviderproduct.Service.OfferService;
import com.vegagroup.apiproviderproduct.dto.OfferDto;
import com.vegagroup.apiproviderproduct.dto.ProductDto;
import com.vegagroup.apiproviderproduct.dto.error.CustomException;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
//import com.easylink.elapiproduct.service.TokenValidationOutputService;
import model.Offer;

@RestController
public class OfferController {

	@Autowired
	private OfferService offerservice;

	Logger logger = LoggerFactory.getLogger(Offer.class);

	// private TokenValditionOutputService tokenValidationOutputService;
	@PostMapping("/v1/provider/Offer/Addoffer")
	@ApiOperation(value = "Ajouter un produit", notes = "Ajout de produit.\n"

			+ "\n<b>result = 0 :</b> Offre Ajouté \n" 
			+ "\n<b>result = -1 :</b> Convention non trouvée \n"

			+ "\n<b>result = 401 :</b> TOKEN NOT AUTHORIZED\n"
			+ "\n<b>result = 402 :</b> TOKEN MISSING.", authorizations = {
					@Authorization(value = "Bearer") }, response = Object.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = Object.class),
			@ApiResponse(code = 401, message = "Unauthorized"), @ApiResponse(code = 403, message = "Forbidden"),
			@ApiResponse(code = 404, message = "not found") })
	public Object saveOffer(@Valid @RequestBody OfferDto offerdto, Integer ConventionId) {

		try {
			return offerservice.AddNewOffer(offerdto, ConventionId);
		} catch (Exception e) {
			logger.error("CustomerEndpoint.getcustomer Exception: " + e.getCause() + " " + e.getMessage());
			return new CustomException(e.getMessage(), e.getCause());
		}

	}
	@PostMapping("/v1/provider/Offer/addproductoffer")
	@ApiOperation(value = "Ajouter un produit à une offre", notes = "Ajout de produit à une offre.\n"

			+ "\n<b>result = 0 :</b> produit Ajouté \n" 
			+ "\n<b>result = -1 :</b> produit existe déja \n"

			+ "\n<b>result = 401 :</b> TOKEN NOT AUTHORIZED\n"
			+ "\n<b>result = 402 :</b> TOKEN MISSING.", authorizations = {
					@Authorization(value = "Bearer") }, response = Object.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = Object.class),
			@ApiResponse(code = 401, message = "Unauthorized"), @ApiResponse(code = 403, message = "Forbidden"),
			@ApiResponse(code = 404, message = "not found") })
	public Object saveproducttooffer(@Valid @RequestBody Integer idOffre, Integer Idproduct) {

		try {
			return offerservice.AddProductToOffre(idOffre, Idproduct);
		} catch (Exception e) {
			logger.error("CustomerEndpoint.getcustomer Exception: " + e.getCause() + " " + e.getMessage());
			return new CustomException(e.getMessage(), e.getCause());
		}

	}

	@PostMapping("/v1/provider/Offer/removeproductoffer")
	@ApiOperation(value = "supprimer un produit d'une offre", notes = "suppression de produit d'une offre.\n"

			+ "\n<b>result = 0 :</b> produit Supprimé \n" 
			+ "\n<b>result = -1 :</b> produit n'existe pas  \n"

			+ "\n<b>result = 401 :</b> TOKEN NOT AUTHORIZED\n"
			+ "\n<b>result = 402 :</b> TOKEN MISSING.", authorizations = {
					@Authorization(value = "Bearer") }, response = Object.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = Object.class),
			@ApiResponse(code = 401, message = "Unauthorized"), @ApiResponse(code = 403, message = "Forbidden"),
			@ApiResponse(code = 404, message = "not found") })
	public Object removeproducttooffer(@Valid @RequestBody Integer idOffre, Integer Idproduct) {

		try {
			return offerservice.RemoveProductFromOffer(idOffre, Idproduct);
		} catch (Exception e) {
			logger.error("CustomerEndpoint.getcustomer Exception: " + e.getCause() + " " + e.getMessage());
			return new CustomException(e.getMessage(), e.getCause());
		}

	}

}
