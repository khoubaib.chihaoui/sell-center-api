package com.vegagroup.apiproviderproduct.mapper;


import model.Category;



import java.util.List;

import com.vegagroup.apiproviderproduct.dto.CategoryDto;



public interface CategoryMapper {
    CategoryDto categoryToCategoryDto(Category category);
    List<CategoryDto> listcategoryTolistCategoryDto(List<Category> categories);
    Category categoryDtoToCategory(CategoryDto categorydto);

}