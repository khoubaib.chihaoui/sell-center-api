package com.vegagroup.apiproviderproduct.mapper;

import java.util.List;

import com.vegagroup.apiproviderproduct.dto.OfferDto;

import model.Offer;

public interface OfferMapper {
	OfferDto offerToOfferDto(Offer offer);
    List<OfferDto> listOfferTolistOfferDto(List<Offer> offers);
    Offer OfferDtoToOffer(OfferDto offerdto);
    
    
}
