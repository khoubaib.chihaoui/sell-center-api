package com.vegagroup.apiproviderproduct.mapper;

import com.vegagroup.apiproviderproduct.dto.BrandDto;

import model.Brand;

public interface BrandMapper {
	
	BrandDto BrandtoBrandDto(Brand brand);
	Brand BrandDtoToBrand(BrandDto brandDto);

}
