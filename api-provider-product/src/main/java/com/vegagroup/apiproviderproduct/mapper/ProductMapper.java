package com.vegagroup.apiproviderproduct.mapper;


import java.util.Set;

import com.vegagroup.apiproviderproduct.dto.ProductDto;

import model.Product;

/**
 * Created by ahmed on 17/07/2019.
 */

public interface ProductMapper {

    ProductDto producToProductDto(Product product);
    Product productDtoToProduct(ProductDto productDto);

    Set<ProductDto> productSetToProductDtoSet(Set<Product> products);
    Set<Product> productDtoSetToProductSet(Set<ProductDto> productDtos);

}
