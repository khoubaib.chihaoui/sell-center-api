package com.vegagroup.apiproviderproduct.Repository;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

import model.Vat;
@Repository
public interface VatRepository extends JpaRepository<Vat ,Integer> {


}
