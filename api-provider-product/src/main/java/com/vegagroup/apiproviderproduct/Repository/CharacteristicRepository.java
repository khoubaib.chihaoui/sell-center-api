package com.vegagroup.apiproviderproduct.Repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import model.Category;
import model.Characteristic;




@Repository
public interface CharacteristicRepository extends JpaRepository<Characteristic, Integer>{

	


	Set<Characteristic> findByCategoryid(Category category);

	

	Set<Characteristic> findBycategoryid(Category category);
}
