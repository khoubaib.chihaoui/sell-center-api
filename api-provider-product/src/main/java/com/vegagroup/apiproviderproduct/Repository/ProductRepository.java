package com.vegagroup.apiproviderproduct.Repository;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

import model.Product;
@Repository
public interface ProductRepository extends JpaRepository<Product ,Integer> {

	Product findProductByProductid(Integer productID);

}
