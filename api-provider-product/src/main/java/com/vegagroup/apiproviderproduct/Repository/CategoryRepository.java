package com.vegagroup.apiproviderproduct.Repository;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

import model.Category;
@Repository
public interface CategoryRepository extends JpaRepository<Category ,Integer> {

}
