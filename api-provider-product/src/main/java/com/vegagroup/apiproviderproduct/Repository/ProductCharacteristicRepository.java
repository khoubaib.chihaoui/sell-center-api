package com.vegagroup.apiproviderproduct.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import model.ProductCharacteristic;





@Repository
public interface ProductCharacteristicRepository extends JpaRepository<ProductCharacteristic, Long>{

}
