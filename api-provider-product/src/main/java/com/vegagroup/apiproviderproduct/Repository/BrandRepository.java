package com.vegagroup.apiproviderproduct.Repository;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

import model.Brand;
@Repository
public interface BrandRepository extends JpaRepository<Brand ,Integer> {

	

}
