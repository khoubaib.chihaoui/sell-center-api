package com.vegagroup.apiproviderproduct.Repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import model.Convention;

/**
 * Created by ahmed on 18/07/2019.
 */
@Repository
public interface ConventionRepository extends JpaRepository<Convention,Integer> {

    public Convention findConventionByConvetionid(Integer conventionID);
}
