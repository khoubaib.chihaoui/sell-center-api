package com.vegagroup.apiproviderproduct.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import model.Amicale;

/**
 * Created by ahmed on 17/07/2019.
 */
@Repository
public interface AmicaleRepository extends JpaRepository<Amicale, Integer> {
    public Amicale findByAmicaleid(Integer amicaleid );
}
