package com.vegagroup.apiproviderproduct.Repository;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

import model.Offer;
@Repository
public interface OfferRepository extends JpaRepository<Offer ,Integer> {

}
